# Image Sequence to yoyo mp4 & GIF

## Requirement:
`ffmpeg` needs to be installed. Check this by typing `ffmpeg` in a terminal.

## Usage:
`pngs2yoyo.sh <png_folder>`

  `<png_folder>` is the folder containing image sequence in png format

## Example:
`./pngs2yoyo.sh ./example/images` will create yoyo.mp4 and yoyo.gif.

The GIF file is typically 10 times larger than mp4. The GIF is also resized to a max width of 1000 px maintaing aspect ratio.

Sample images: 3D projections 1-degree apart generated from confocal stack of some interneurons in antennal lobe of drosophila using ImageJ.
Image credit: Ankita Chodankar

![](example/images/yoyo.gif)
