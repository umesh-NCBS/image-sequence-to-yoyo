#! /bin/bash
cd $1

touch images v forward.mp4 reverse.mp4 yoyo.mp4 yoyo.gif
rm images v forward.mp4 reverse.mp4 yoyo.mp4 yoyo.gif
for f in *.png
do
  echo file \'$f\' >> images
done
echo "file 'forward.mp4'" > v
echo "file 'reverse.mp4'" >> v

ffmpeg -r 20 -f concat -safe 0 -i images -c:v libx264 -pix_fmt yuv420p -crf 17 forward.mp4
ffmpeg -i forward.mp4 -vf reverse -c:v libx264 -pix_fmt yuv420p -crf 17 reverse.mp4
ffmpeg -f concat -safe 0 -i v -c:v copy yoyo.mp4
rm images v forward.mp4 reverse.mp4

palette="/tmp/palette.png"
filters="fps=20,scale=1000:-1:flags=lanczos"

ffmpeg -i yoyo.mp4 -vf "$filters,palettegen=stats_mode=diff" -y $palette
ffmpeg -i yoyo.mp4 -i $palette -lavfi "$filters [x]; [x][1:v] paletteuse=dither=floyd_steinberg" -y yoyo.gif
